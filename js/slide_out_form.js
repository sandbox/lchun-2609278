(function ($) {

  "use strict";

  Drupal.behaviors.slideoutform = {
    attach: function (context) {

      var tabheight = Drupal.settings.slideoutform.tabheight;
      var tabwidth = Drupal.settings.slideoutform.tabwidth;

      $.fn.rotate = function(degrees, origin) {
          $(this).css({'-webkit-transform' : 'rotate('+ degrees +'deg)',
                        '-webkit-transform-origin' : origin,
                        '-moz-transform' : 'rotate('+ degrees +'deg)',
                        '-moz-transform-origin' : origin,
                        '-ms-transform' : 'rotate('+ degrees +'deg)',
                        '-ms-transform-origin' : origin,
                        '-o-transform' : 'rotate('+ degrees +'deg)',
                        '-o-transform-origin' : origin,
                        'transform' : 'rotate('+ degrees +'deg)',
                        'transform-origin' : origin
                      });
          return $(this);
      };
      
      if($(".slideout-container").length){

        var pos = Drupal.settings.slideoutform.position;
        var formheight = Drupal.settings.slideoutform.formheight;
        var formwidth = Drupal.settings.slideoutform.formwidth;

        //Briefly show and hide the container so that jquery can measure the tab
        // The user shouldn't see this happening
        $(".slideout-container").show();
        var slideOutSpanHeight = $(".slideout-slide-tab span").height();
        $(".slideout-container").hide();

        $(".slideout-slide-tab").css('height', tabheight +'px');
        $(".slideout-slide-tab").css('width', tabwidth + 'px');
        $(".slideout-slide-tab span").css('margin-top', 0 - (slideOutSpanHeight / 2) + 'px');
        $(".slideout-slide-tab").css('display', 'block');
        $(".slideout-slide-tab").css('position', 'relative');

        $(".slideout-container").css('position', 'fixed');
        $(".slideout-container").css('top', '0px');

        $(".slideout-form").css('width', formwidth + 'px');
        $(".slideout-form").css('height', formheight + 'px');
        $(".slideout-form").css('overflow', 'auto');

        var containerWidth = $('.slideout-form').outerWidth() + 'px';
        $(".slideout-container").css('width', containerWidth);

        if(pos == 'right'){
          $(".slideout-container").css('right', '-' + containerWidth);
          $(".slideout-slide-tab").css('left', 'auto');
          $(".slideout-slide-tab").css('right', tabwidth + 'px');
          $(".slideout-slide-tab").rotate(-90, 'bottom right');
        } else if(pos == 'left'){
          $(".slideout-container").css('left', '-' + containerWidth);
          $(".slideout-slide-tab").css('right', 'auto');
          $(".slideout-slide-tab").css('left', containerWidth);
          $(".slideout-slide-tab").rotate(90, 'bottom left');
        } else if(pos == 'bottom'){
          $(".slideout-container").css('bottom', '-' + $(".slideout-form").outerHeight() + 'px');
          $(".slideout-container").css('top', 'auto');
          $(".slideout-slide-tab").css('right', '0px');
          $(".slideout-slide-tab").css('left', 'auto');
          $(".slideout-slide-tab").css('bottom', '0px');
        }

        $(".slideout-slide-tab").click(function(){

          var slideoutMenu = $('.slideout-container');
          var slideoutMenuWidth = $('.slideout-container').width();
          if(pos == 'bottom'){
            slideoutMenuWidth = $('.slideout-form').outerHeight();
          }
          
          // toggle open class
          slideoutMenu.toggleClass("open");
          
          // slide menu
          if (slideoutMenu.hasClass("open")) {
            var a = {};
            a[pos] = "0px";
            slideoutMenu.animate(a, 250); 
          } else {
            var a = {};
            a[pos] = -slideoutMenuWidth;
            slideoutMenu.animate(a, 250);  
          }
        });

        $(".slideout-container").addClass('processed');

      } else if($(".slideout-modal-tab").length){
        var pos = Drupal.settings.slideoutform.position;

        if(pos == 'right'){
          $(".slideout-modal-tab").css({
            'top': '0px',
            'right': '0px',
            'width': tabwidth + 'px',
            'height': tabheight + 'px',
            'position': 'fixed'
          });
          $(".slideout-modal-tab").rotate(-90, 'bottom right');
        } else if(pos == 'left'){
          $(".slideout-modal-tab").css({
            'top': '0px',
            'left': '0px',
            'width': tabwidth + 'px',
            'height': tabheight + 'px',
            'position': 'fixed'
          });
          $(".slideout-modal-tab").rotate(90, 'bottom left');
        } else if(pos == 'bottom'){
          $(".slideout-modal-tab").css({
            'bottom': '0px',
            'width': tabwidth + 'px',
            'height': tabheight + 'px',
            'position': 'fixed'
          });
        }

        $(".slideout-modal-tab").addClass('processed');

      }
    }
  };
})(jQuery);